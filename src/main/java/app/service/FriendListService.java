package app.service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Queue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.entities.FriendList;
import app.entities.Users;
import app.repository.FriendListRepository;
import app.repository.UsersRepository;
import app.requestBean.KthFriendInfoBean;

@Service
public class FriendListService {
	@Autowired
	private UsersRepository usersRepo;
	
	@Autowired
	private FriendListRepository friendListRepo;
	
	
	public List<Users> getMatualFriendList(long friendUserId,long userId){
		List<Users> matualfriendList = new ArrayList<Users>();
		List<FriendList> friendUserIdList=friendListRepo.findByUserId(friendUserId);
		List<FriendList> ownfriendUserIdList=friendListRepo.findByUserId(userId);
		for(FriendList flData:friendUserIdList) {
			for(FriendList ownFlData:ownfriendUserIdList) {
				if(flData.getFriendUserId()==ownFlData.getFriendUserId()) {
					Optional<Users> optionalUser= usersRepo.findById(ownFlData.getFriendUserId());
					Users u = null;
					if (optionalUser.isPresent()) {
						u=optionalUser.get();
						matualfriendList.add(u);
					}
					
				}
				
			}
		}
		return matualfriendList;
		
	}
	public List<Users> getKthLevelFriendList(long k,long userId){
		List<Users> friendList = new ArrayList<Users>();
		return this.getKthLevelUsingLevelOrderTravel(friendList,k, userId);
		
	}
	private List<Users> getKthLevelUsingLevelOrderTravel(List<Users> friendList,long k,long userId){
		List<Long> visitedUserIds= new ArrayList<Long>();
		Optional<Users> optionalUser= usersRepo.findById(userId);
		Users u = null;
		if (optionalUser.isPresent()) {
			u=optionalUser.get();
		}

		visitedUserIds.add(u.getId());
		Queue<KthFriendInfoBean> dataQueue= new LinkedList<KthFriendInfoBean>();
		KthFriendInfoBean kthFriendInfoBean = new KthFriendInfoBean();
		kthFriendInfoBean.setDistance(0);
		kthFriendInfoBean.setVisited(1);
		kthFriendInfoBean.setUserId(u.getId());
		
		dataQueue.add(kthFriendInfoBean);
		while(!dataQueue.isEmpty()) {
			KthFriendInfoBean	data=dataQueue.poll();
			if (data.getDistance() == k) {
				Optional<Users> usroptional = usersRepo.findById(data.getUserId());
				if (usroptional.isPresent()) {
					friendList.add(usroptional.get());
				}

			}
			List<FriendList> upcomingFriendList=friendListRepo.findByUserId(data.getUserId());
			for(FriendList fl :upcomingFriendList) {
				if(!visitedUserIds.contains(fl.getFriendUserId())) {
					KthFriendInfoBean friendBean = new KthFriendInfoBean();
					friendBean.setDistance(data.getDistance()+1);
					friendBean.setVisited(1);
					friendBean.setUserId(fl.getFriendUserId());
					dataQueue.add(friendBean);
					visitedUserIds.add(fl.getFriendUserId());
					
				}
				
			}
			
		}
		
		return friendList;
		
	}
	

}
