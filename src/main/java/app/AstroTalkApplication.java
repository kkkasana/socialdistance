package app;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;


@SpringBootApplication
@PropertySource("classpath:application.properties")
public class AstroTalkApplication {

	public static void main(String[] args) {
		SpringApplication.run(AstroTalkApplication.class, args);
	

	}

}
