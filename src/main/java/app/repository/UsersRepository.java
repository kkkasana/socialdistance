package app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import app.entities.Users;



public interface UsersRepository extends JpaRepository<Users, Long> {
    Users findById(int userId);

}
