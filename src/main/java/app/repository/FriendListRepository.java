package app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import app.entities.FriendList;



public interface FriendListRepository extends JpaRepository<FriendList, Long> {
	List<FriendList> findByUserId(long userId);
	FriendList findByUserIdAndFriendUserId(long userId,long friendUserId);


}
