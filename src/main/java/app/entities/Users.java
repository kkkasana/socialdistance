package app.entities;
import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;

@Entity
@Data
@Table(name = "users")
public class Users {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "native")
	private long id;
	private String firstName;
	private String lastName;
	private String userName;
	private String email;
	private String mobile;
	private String status;
	@CreationTimestamp
	private Calendar createdAt;
	@UpdateTimestamp
	private Calendar updatedAt;
	
	public static final String STATUS_ACTIVE = "ACTIVE";
    public static final String STATUS_INACTIVE = "INACTIVE";
    public static final String STATUS_DEACTIVATED = "DEACTIVATED";

}
