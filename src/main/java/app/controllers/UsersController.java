package app.controllers;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import app.entities.Users;
import app.repository.UsersRepository;
import app.requestBean.UsersBean;



@RestController
@RequestMapping("api/users/")
public class UsersController {
	
	@Autowired 
	private UsersRepository usersRepo;
	
	@PostMapping("/createUser")
    public String createUsers(@RequestBody UsersBean usersBean) throws Exception {
		Users u= new Users();
		u.setCreatedAt(Calendar.getInstance());
		u.setEmail(usersBean.getEmail());
		u.setFirstName(usersBean.getFirstName());
		u.setLastName(usersBean.getLastName());
		u.setMobile(usersBean.getMobile());
		u.setStatus(Users.STATUS_ACTIVE);
		u.setUpdatedAt(Calendar.getInstance());
		u.setUserName(usersBean.getUserName());
	    usersRepo.save(u);
		return "user create successfully with userId= "+u.getId();
        
    }
	@GetMapping("/getAllUsers")
    public List<Users> createUsers() throws Exception {
		
		return usersRepo.findAll();
        
    }
	@GetMapping("/deleteuser")
	public String deleteuser(@RequestParam long userId) throws Exception {
		Optional<Users> optionalUser = usersRepo.findById(userId);
		if (optionalUser.isPresent()) {
			usersRepo.delete(optionalUser.get());
			return "user deleted successfully";
		} else {
			return "user not found";
		}

	}
	
	
	
	
	

}
