package app.controllers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import app.entities.FriendList;
import app.entities.Users;
import app.repository.FriendListRepository;
import app.repository.UsersRepository;
import app.requestBean.FriendRequestBean;
import app.service.FriendListService;

@RestController
@RequestMapping("api/friendList/")
public class FriendListController {
	

	@Autowired 
	private FriendListRepository friendListRepo;
	
	@Autowired
	private FriendListService friendListService;
	
	@Autowired
	private UsersRepository usersRepo;
	
	@PostMapping("/addFriend")
    public String addFriend(@RequestBody FriendRequestBean friendRequestBean) throws Exception {
		FriendList friendList=friendListRepo.findByUserIdAndFriendUserId(friendRequestBean.getUserId(), 
				friendRequestBean.getFriendUserId());
		if(friendList!=null ) {
			return "given user id and friend user Id is already in friendList";
		}
	  // A added B as his friend
		FriendList flAB= new FriendList();
		flAB.setCreatedAt(Calendar.getInstance());
		flAB.setFriendUserId(friendRequestBean.getFriendUserId());
		flAB.setStatus(FriendList.STATUS_ACTIVE);
		flAB.setUpdatedAt(Calendar.getInstance());
		flAB.setUserId(friendRequestBean.getUserId());
		friendListRepo.save(flAB);
		
		// B added A as his friend also
		FriendList flBA= new FriendList();
		flBA.setCreatedAt(Calendar.getInstance());
		flBA.setFriendUserId(friendRequestBean.getUserId());
		flBA.setStatus(FriendList.STATUS_ACTIVE);
		flBA.setUpdatedAt(Calendar.getInstance());
		flBA.setUserId(friendRequestBean.getFriendUserId());
		friendListRepo.save(flBA);
		
		return "friend added successfully with friendListId = "+flAB.getId();
        
    }
	@PostMapping("/removeFriend")
	public String removeFriend(@RequestBody FriendRequestBean friendRequestBean) throws Exception {
		FriendList friendListAB = friendListRepo.findByUserIdAndFriendUserId(friendRequestBean.getUserId(),
				friendRequestBean.getFriendUserId());

		if (friendListAB == null) {
			return "given user id and friend user Id is not found in friendList";
		} else {
			friendListRepo.delete(friendListAB);

		}
		FriendList friendListBA = friendListRepo.findByUserIdAndFriendUserId(friendRequestBean.getFriendUserId(),
				friendRequestBean.getUserId());
		if (friendListBA == null) {
			return "given user id and friend user Id is not found in friendList";
		} else {
			friendListRepo.delete(friendListBA);

		}
		return "friends remove successfully ";

	}
	
	
	@GetMapping("/get_Kth_level_friend_list")
    public List<Users> getKthLevelFriendList(@RequestParam long k,@RequestParam long userId) throws Exception {
		
		return friendListService.getKthLevelFriendList(k, userId);
	}
	@GetMapping("/get_matual_friend_list")
    public List<Users> getMatualFriendList(@RequestParam long friendUserId,@RequestParam long userId) throws Exception {
		
		return friendListService.getMatualFriendList(friendUserId, userId);
	}
	@GetMapping("/get_friend_list")
	public List<Users> getMatualFriendList(@RequestParam long userId) throws Exception {
		List<FriendList> friendUserIdList = friendListRepo.findByUserId(userId);
		List<Users> friendList = new ArrayList<Users>();
		for (FriendList fl : friendUserIdList) {
			Optional<Users> optionalUser = usersRepo.findById(fl.getFriendUserId());
			Users u = null;
			if (optionalUser.isPresent()) {
				u = optionalUser.get();
				friendList.add(u);
			}

		}

		return friendList;
	}

}
