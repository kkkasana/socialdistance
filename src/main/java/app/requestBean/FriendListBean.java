package app.requestBean;
import lombok.Data;

@Data
public class FriendListBean {
	private long userId;
	private long friendUserId;
	private String status;

}
