package app.requestBean;

import lombok.Data;

@Data
public class FriendRequestBean {
	private long userId;
	private long friendUserId;
	private String status;

}
