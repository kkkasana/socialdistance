package app.requestBean;

import lombok.Data;

@Data
public class KthFriendInfoBean {
	private long userId;
	private long distance;
	private int visited=0;

}
