package app.requestBean;
import lombok.Data;

@Data
public class UsersBean {
	private long id;
	private String firstName;
	private String lastName;
	private String userName;
	private String email;
	private String mobile;
	private String status;
	
	

}
